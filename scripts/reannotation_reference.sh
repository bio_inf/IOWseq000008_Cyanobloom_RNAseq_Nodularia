# re-annotation of reference genome with KEGG and InterProScan

cd /bio/Analysis_data/IOWseq000008_Cyanobloom_RNAseq_Nodularia/Intermediate_results/Reference_genome

# KEGG
conda activate dbcan-3.0.1
mkdir kegg_out
diamond blastp -d /bio/Databases/KEGG/release_20220124/diamond/kegg_genes.dmnd --sensitive -e 1e-5 -q GCF_000340565.2_ASM34056v3_protein.faa --top 10 -p 48 -o kegg_out/out_kegg_diamond.txt -f 6
diamond makedb --in GCF_000340565.2_ASM34056v3_protein.faa -d kegg_out/cds_db
diamond blastp -d kegg_out/cds_db.dmnd -q GCF_000340565.2_ASM34056v3_protein.faa -k 1 -o kegg_out/out_self_diamond.txt -f 6
conda activate r-4.1.0
/bio/Common_repositories/workflow_templates/metaG_Illumina_PE/scripts/parse_kegg.R -b kegg_out/out_kegg_diamond.txt -s kegg_out/out_self_diamond.txt -m /bio/Databases/KEGG/release_20220124/mapping_info -c 0.4 -t 2 -o kegg_out/out_kegg_parsed
# rescue best hits without KO number
sed '1d' kegg_out/out_kegg_parsed_best.txt | awk -v FS="\t" -v OFS="\t" '$14 == ""' | cut -f1 | grep -w -F -f - kegg_out/out_kegg_parsed.txt | awk -v FS="\t" -v OFS="\t" '$14 != ""' > kegg_out/out_kegg_rescue_noKO.txt

# Also run kofamscan
conda activate kofamscan-1.3.0
mkdir kofam_out
exec_annotation -o kofam_out/out_kofamscan.txt -p /bio/Databases/KOfam/Dec2021/kofamscan/profiles -k /bio/Databases/KOfam/Dec2021/kofamscan/ko_list --tmp-dir=tmp_kofam --cpu 12 -E 0.01 GCF_000340565.2_ASM34056v3_protein.faa
grep "^\*" kofam_out/out_kofamscan.txt | sed 's/^\* //' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' | sed -E 's/ +/\t/' > kofam_out/out_kofamscan_parsed.txt
rm -rf tmp_kofam

# InterProScan
conda activate base
module load interproscan/5.55-88.0
interproscan.sh -i GCF_000340565.2_ASM34056v3_protein.faa -b GCF_000340565.2_ASM34056v3_iprout -goterms -cpu 8 -T tmp_ipr -f TSV >> interproscan.log 2>&1
rm -rf tmp_ipr

# SignalP and TMHMM not working within InterProScan
# Use web server: https://services.healthtech.dtu.dk/service.php?SignalP
# TMHMM I could not get to run outside of metaerg --> using web server instead: https://dtu.biolib.com/DeepTMHMM

# Manually explore interactions on STRING

# diamond sensitive against nr (exclude Nodularia spumingena CCY9414 hits)
conda activate dbcan-3.0.1
mkdir ncbi_out
# without strain in results
diamond blastp -d /bio/Databases/NCBI_nr/20210926/diamond/diamond_nr.dmnd --sensitive -e 1e-5 -q GCF_000340565.2_ASM34056v3_protein.faa --top 20 -p 80 -o ncbi_out/out_ncbi_diamond.txt -f 6 --taxon-exclude 313624 -b 8
# without species in results
# diamond blastp -d /bio/Databases/NCBI_nr/20210926/diamond/diamond_nr.dmnd --sensitive -e 1e-5 -q GCF_000340565.2_ASM34056v3_protein.faa --top 10 -p 80 -o ncbi_out/out_ncbi_diamond.txt -f 6 --taxon-exclude 70799,128405,88833,159350,165801,258823,267927,284707,284708,284709,284710,284711,284712,284713,284714,284715,284716,284717,300279,300280,300281,300282,300283,300284,313624,380550,541231,582745,857314,1051977,1095240,1097562,1194170,1246646,1344964,1346284,1346285,1378556,1388901,1388902,1388903,1574318,1819295,1914872,1915166,1915167,1926402,1926403,1926404,1955788,1955789,1955790,1955791,1955792,1955793,1955794,1955795,1955796,1955797,1955798,1955799,1955800,1955801,1955802,1955803,2027345,2027346,2027347,2161834,2161835,2161836,2161837,2161838,2161839,2161840,2161841,2161842,2301359,2739192 -b 8

# parse ncbi diamond output: restrict to hypothetical proteins and bsr 0.4
grep "hypothetical" ../GCF_000340565.2_ASM34056v3_protein.faa | sed -e 's/^>//' -e 's/ .*//' 
# in R:
# setwd("C:/Users/chassenrueck/Documents/IOW_home/IOWseq_projects/IOWseq000008_Cyanobloom_RNAseq_Nodularia/dea_final/ncbi_nr")
# require(rentrez)
# require(XML)
# require(data.table)
# require(tidyverse)
# selfout <- fread(
#   "../kegg_out/out_self_diamond.txt",
#   h = F,
#   sep = "\t",
#   nThread = 10,
#   col.names = c(
#     "qseqid",
#     "sseqid",
#     "pident",
#     "length",
#     "mismatch",
#     "gapopen",
#     "qstart",
#     "qend",
#     "sstart",
#     "send",
#     "evalue",
#     "bitscore"
#   )
# )[, c("qseqid", "bitscore")]
# setkey(selfout, "qseqid")
# blastout <- fread(
#   "out_ncbi_hypothetical.txt",
#   h = F,
#   sep = "\t",
#   nThread = 10,
#   col.names = c(
#     "qseqid",
#     "sseqid",
#     "pident",
#     "length",
#     "mismatch",
#     "gapopen",
#     "qstart",
#     "qend",
#     "sstart",
#     "send",
#     "evalue",
#     "bitscore"
#   )
# ) %>% 
#   mutate(
#     bsr = round(.$bitscore/selfout[.$qseqid, "bitscore"], 4) %>% pull(1)
#   ) %>% 
#   filter(
#     bsr >= 0.4
#   )
# blastout$stitle <- rep(NA, nrow(blastout))
# for(i in 1:nrow(blastout)) {
#   if(i %in% seq(1, nrow(blastout), 1000)) print(i)
#   tmp <- entrez_fetch("protein", id = blastout$sseqid[i], rettype = "fasta", retmode = "text", parsed = F)
#   blastout$stitle[i] <- gsub(paste0(">", blastout$sseqid[i], " "), "", strsplit(tmp, "\n")[[1]][1])
# }
# fwrite(
#   blastout,
#   "out_ncbi_hypothetical_stitle.txt",
#   quote = F,
#   sep = "\t",
#   nThread = 10,
#   col.names = T,
#   row.names = F
# )
# 300 proteins were assigned hits not containing hypothetical in their names


# PSORTb?, AGNOSTOS-DB?