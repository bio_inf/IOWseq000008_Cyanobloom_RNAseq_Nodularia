### testing workflow for transcriptome analysis ###

WDIR="/bio/Analysis_data/IOWseq000008_Cyanobloom_RNAseq_Nodularia"
NAME="v1"

mkdir $WDIR/Intermediate_results/$NAME
mkdir $WDIR/Intermediate_results/$NAME/Trimmed
mkdir $WDIR/Intermediate_results/$NAME/Mapped
mkdir $WDIR/Intermediate_results/$NAME/Coverage

# list of sample names
ls -1 $WDIR/Validated_data/*.gz | xargs -n 1 basename | sed 's/\.fastq.gz//' > $WDIR/Intermediate_results/sample_names.txt

# quality trimming
# assuming that adapters were already removed
conda activate bbmap-38.90
cat $WDIR/Intermediate_results/sample_names.txt | while read line
do
  bbduk.sh in=$WDIR/Validated_data/${line}.fastq.gz out=$WDIR/Intermediate_results/$NAME/Trimmed/${line}_trim.fastq.gz qtrim=w,4 trimq=15 minlength=50 trimpolyg=10 threads=50 fastawrap=300
done
conda deactivate

# read mapping
conda activate coverm-0.6.1
bwa index $WDIR/Intermediate_results/Reference_genome/GCF_000340565.2_ASM34056v3_genomic.fna
cat $WDIR/Intermediate_results/sample_names.txt | while read line
do
  bwa mem -v 1 -t 32 $WDIR/Intermediate_results/Reference_genome/GCF_000340565.2_ASM34056v3_genomic.fna $WDIR/Intermediate_results/$NAME/Trimmed/${line}_trim.fastq.gz > $WDIR/Intermediate_results/$NAME/Mapped/${line}.sam
  samtools sort -T $WDIR/Intermediate_results/$NAME/Mapped/tmp_${line}_samtools -@ 32 -O BAM -o $WDIR/Intermediate_results/$NAME/Mapped/${line}.bam $WDIR/Intermediate_results/$NAME/Mapped/${line}.sam
  rm $WDIR/Intermediate_results/$NAME/Mapped/${line}.sam
done
conda deactivate

# remove rRNA from bam
# bedtools intersect
grep "ribosomal RNA" $WDIR/Intermediate_results/Reference_genome/GCF_000340565.2_ASM34056v3_genomic.gff > $WDIR/Intermediate_results/Reference_genome/GCF_000340565.2_ASM34056v3_genomic_rRNA.gff
conda activate phyloflash-3.4
cd $WDIR/Intermediate_results/$NAME
cat ../sample_names.txt | parallel -j12 'bedtools intersect -v -a Mapped/{}.bam -b ../Reference_genome/GCF_000340565.2_ASM34056v3_genomic_rRNA.gff > Mapped/{}_no_rRNA.bam'
conda deactivate

# filter output to have higher confidence in mappings: no supplementary or secondary alignments, minimum 50bp aligned to reference (part of CIGAR string)
conda activate coverm-0.6.1
cat ../sample_names.txt | parallel -j12 'samtools view -F2304 -m50 -b Mapped/{}_no_rRNA.bam > Mapped/{}_filt.bam'

# remove mappings with less than 95% similarity to reference genome
cat ../sample_names.txt | while read SID
do
  samtools view -h Mapped/${SID}_filt.bam > Mapped/tmp.sam
  grep -v '^@' Mapped/tmp.sam > Mapped/tmp_seqs.txt
  grep '^@' Mapped/tmp.sam > Mapped/tmp_${SID}.sam
  paste <(sed -e 's/^.*MD:Z://' -e 's/\t.*$//' Mapped/tmp_seqs.txt | sed -r 's/[^0-9]+/\+/g' | bc -l) <(cut -f10 Mapped/tmp_seqs.txt | perl -nle 'print length') | paste - Mapped/tmp_seqs.txt | awk -v threshold=95 '$1/$2 * 100 >= threshold' | cut -f3- >> Mapped/tmp_${SID}.sam
  samtools sort -T tmp_samtools -@ 32 -O BAM -o Mapped/${SID}_final.bam Mapped/tmp_${SID}.sam
  rm Mapped/tmp.sam Mapped/tmp_seqs.txt Mapped/tmp_${SID}.sam
done
conda deactivate

# get read counts per feature
conda activate phyloflash-3.4
cat ../sample_names.txt | parallel -j12 'bedtools coverage -counts -a ../Reference_genome/GCF_000340565.2_ASM34056v3_genomic.gff -b Mapped/{}_final.bam > Coverage/{}_counts.txt'
conda deactivate

# get coverage per bp for selected genome range (nsp7580-7450)
cd $WDIR/Intermediate_results/$NAME
conda activate phyloflash-3.4
# extract relevant genes from gff
grep -B2 -A29 "NSP_7450" ../Reference_genome/GCF_000340565.2_ASM34056v3_genomic.gff | awk '$2 == "RefSeq"' > ../Reference_genome/nsp7440_nsp7590.gff
# based on start and end positions define .bed file
cat  > ../Reference_genome/A.bed # NZ_CP007203.1	741294	754872
# get coverage
cat ../sample_names.txt | parallel -j12 'bedtools coverage -d -a ../Reference_genome/A.bed -b Mapped/{}_final.bam > Coverage/{}_depth_nsp7440_nsp7590.txt'
# combine output
cd Coverage
cp T14_minus_P1_depth_nsp7440_nsp7590.txt tmp
ls -1 *_nsp7440_nsp7590.txt | sed '1d' | while read line
do
  cut -f5 $line | paste tmp - > tmp1
  mv tmp1 tmp
done
echo -e 'chromosome\tstart\tend\tposition\tT14_minus_P1\tT14_minus_P2\tT14_minus_P3\tT14_plus_P1\tT14_plus_P2\tT14_plus_P3\tT7_minus_P1\tT7_minus_P2\tT7_minus_P3\tT7_plus_P1\tT7_plus_P2\tT7_plus_P3' > depth_nsp7440_nsp7590_all.txt
cat tmp >> depth_nsp7440_nsp7590_all.txt

# get sequencing effort from filtered bam files
cat ../sample_names.txt | parallel -j12 'bedtools genomecov -ibam Mapped/{}_filt.bam -d > Coverage/{}_aln.txt'
cat ../sample_names.txt | parallel -j12 'cut -f3 Coverage/{}_aln.txt | paste -sd+ | bc -l | paste <(echo "{}") - ' > Coverage/aligned_bases_summary.txt